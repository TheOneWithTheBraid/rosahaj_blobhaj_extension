#!/usr/bin/env bash

set -e

rm -rf output/png/{base,flags,cathode,scooter}

mkdir output/png/base
mkdir output/png/flags
mkdir output/png/cathode
mkdir output/png/scooter

find output/svg -name "*.svg" | while read -r file; do magick -background transparent -density 288 $file ${file//svg/png}; done 

