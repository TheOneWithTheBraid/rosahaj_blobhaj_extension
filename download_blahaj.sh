#!/usr/bin/env bash

set -e

USER_AGENT="Mozilla/5.0 (X11; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/121.0"

BASE_URL="https://heatherhorns.com"
WP_UPLOADS="${BASE_URL}/wp-content/uploads"

BASE="${WP_UPLOADS}/2022/12/Blobhaj-12-13-2022.zip"
FLAGS="${BASE_URL}/BlobhajFlags.zip"
CATHODE="${WP_UPLOADS}/2022/12/Blahaj_for_CathodeChurch.zip"
SCOOTER="${WP_UPLOADS}/2024/05/Blobhaj-Scooter.zip"

WORKDIR="$(mktemp -d)"

pushd "${WORKDIR}"

echo $BASE

curl -A "${USER_AGENT}" -L -o base.zip "${BASE}"
mkdir base
unzip base.zip "SVG/*" -d base

curl -A "${USER_AGENT}" -L -o flags.zip "${FLAGS}"
mkdir flags
unzip flags.zip "SVG/*" -d flags

curl -A "${USER_AGENT}" -L -o cathode.zip "${CATHODE}"
mkdir cathode
unzip cathode.zip "SVG/*" -d cathode

curl -A "${USER_AGENT}" -L -o scooter.zip "${SCOOTER}"
mkdir scooter
unzip scooter.zip "*.svg" -d scooter

popd

rm -rf "blobhaj_input/base"
mv "${WORKDIR}/base/SVG" "blobhaj_input/base"

rm -rf "blobhaj_input/flags"
mv "${WORKDIR}/flags/SVG" "blobhaj_input/flags"

rm -rf "blobhaj_input/cathode"
mv "${WORKDIR}/cathode/SVG" "blobhaj_input/cathode"

rm -rf "blobhaj_input/scooter"
mv "${WORKDIR}/scooter" "blobhaj_input/scooter"

rm -rf "${WORKDIR}"

