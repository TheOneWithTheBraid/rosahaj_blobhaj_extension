# Rosahaj extension for Blobhaj emojis

Converts the beautiful [Blobhaj emote packs](https://heatherhorns.com/emoji/) to Rosahaj branded ones.

## Usage

Requirements :

- `convert` command from ImageMagick
- `curl`
- `unzip`

```shell
# download the upstream Blobhaj emojis
./download_blahaj.sh
# convert the SVGs to Rosa pink shades
./convert_blahaj_to_rosahaj.sh
# convert to pink PNG files
./convert_png.sh
```

## Download

You can get the latest converted version from the CI jobs.

## Rosahaj statement

Rosahaj sais this is all hers, and she's the best haj. You can follow her on the Fedi at [@rosahaj@blahaj.social](https://blahaj.social/@rosahaj).

## Credits

These script would not be possible without the wonderful design work of heatherhorns ! Consider donating them.

## License and attribution

Both the upstream Blobhaj emojis and this work are licensed under the terms and condition of [CC BY-SA](LICENSE).

