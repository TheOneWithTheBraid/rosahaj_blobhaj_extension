#!/usr/bin/env bash

set -e

rm -rf output/svg/*.svg
cp -r blobhaj_input/* output/svg/

find output/svg -name "*.svg" -exec sed -i 's/#54899b/#ff93d6/g' {} +
find output/svg -name "*.svg" -exec sed -i 's/#48588e/#ff3eb5/g' {} +
find output/svg  -name "*.svg" -exec sed -i 's/#55899b/#ff93d6/g' {} +
find output/svg -name "*.svg" -exec sed -i 's/#558b9c/#ff93d6/g' {} +
find output/svg -name "*.svg" -exec sed -i 's/#55889a/#ff93d6/g' {} +
find output/svg -name "*.svg" -exec sed -i 's/#558a9c/#ff93d6/g' {} +
find output/svg -name "*.svg" -exec sed -i 's/#4f668e/#ff3eb5/g' {} +
find output/svg -name "*.svg" -exec sed -i 's/#578c9b/#ff93d6/g' {} +

for i in output/svg/**/*Blobhaj*.svg; do mv "$i" "${i/Blobhaj/Rosahaj}"; done
for i in output/svg/**/*Blahaj*.svg; do mv "$i" "${i/Blahaj/Rosahaj}"; done
for i in output/svg/**/*BhjFlag*.svg; do mv "$i" "${i/BhjFlag/Rosahaj_Flag}"; done
